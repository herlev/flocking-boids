# Flocking boids

![demo](demo.gif)

## Running the application
[Install rust and cargo](https://www.rust-lang.org/tools/install), followed by:

`cargo run --release`
