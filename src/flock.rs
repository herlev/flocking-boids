use std::ops::Range;

use itertools::Itertools;
use nannou::{geom::Rect, prelude::Vec2, rand::random_range, Draw};
use rand::{rngs::StdRng, Rng};
use rayon::prelude::*;

use crate::boid::Boid;

fn random_vec(min: Vec2, max: Vec2) -> Vec2 {
  Vec2::new(random_range(min.x, max.x), random_range(min.y, max.y))
}

fn random_vec_rng(min: Vec2, max: Vec2, rng: &mut impl Rng) -> Vec2 {
  Vec2::new(rng.gen_range(min.x..max.x), rng.gen_range(min.y..max.y))
}

fn wrap1d(num: f32, range: Range<f32>) -> f32 {
  // Should probably not set to min and max directly here
  // instead set it to min/max + an offset
  if num > range.end {
    range.start
  } else if num < range.start {
    range.end
  } else {
    num
  }
}

fn wrap_vec(mut v: Vec2, min: Vec2, max: Vec2) -> Vec2 {
  v.x = wrap1d(v.x, min.x..max.x);
  v.y = wrap1d(v.y, min.y..max.y);
  v
}

pub struct Flock {
  num_boids: u32,
  vision_radius: f32,
  pub boids: Vec<Boid>,
  pub k_separation: f32,
  pub k_alignment: f32,
  pub k_cohesion: f32,
  pub sync_activation_period: f32,
  pub sync_coupling_constant: f32,
  pub bbox: Rect,
  pub should_flock: bool,
  last_non_flocking_time: f32,
}

impl Flock {
  pub fn reset(&mut self, current_time: f32) {
    let mut boids = Vec::new();
    for _ in 0..self.num_boids {
      let b = Boid {
        position: random_vec(self.bbox.bottom_left(), self.bbox.top_right()),
        velocity: random_vec(-Vec2::ONE, Vec2::ONE).normalize() * 200.,
        activation: random_range(0., 1.),
        vision_radius: self.vision_radius,
        is_flashing: false,
      };
      boids.push(b);
    }
    self.boids = boids;
    self.last_non_flocking_time = current_time;
  }
  pub fn new(
    num_boids: u32,
    vision_radius: f32,
    k_separation: f32,
    k_alignment: f32,
    k_cohesion: f32,
    sync_activation_period: f32,
    sync_coupling_constant: f32,
    bbox: Rect,
    current_time: f32,
    rng: Option<impl Rng>,
  ) -> Self {
    // let rng = rng.unwrap_or(nannou::rand::rand::thread_rng());
    let mut rng = rng.unwrap();
    let mut boids = Vec::new();
    for _ in 0..num_boids {
      let b = Boid {
        position: random_vec_rng(bbox.bottom_left(), bbox.top_right(), &mut rng),
        velocity: random_vec_rng(-Vec2::ONE, Vec2::ONE, &mut rng).normalize() * 200.,
        activation: rng.gen_range(0.0..1.),
        vision_radius,
        is_flashing: false,
      };
      boids.push(b);
    }
    Self {
      num_boids,
      vision_radius,
      boids,
      k_separation,
      k_alignment,
      k_cohesion,
      sync_activation_period,
      sync_coupling_constant,
      bbox,
      last_non_flocking_time: current_time,
      should_flock: true,
    }
  }
  pub fn update(&mut self, dt: f32, current_time: f32) {
    let old_boids = self.boids.clone();

    self.boids.par_iter_mut().enumerate().for_each(|(i, boid)| {
      let others = old_boids
        .clone()
        .into_iter()
        .enumerate()
        .filter_map(|(j, b)| if i == j { None } else { Some(b) })
        .map(|other| boid.get_local_boid(other, self.bbox.w(), self.bbox.h()))
        .collect_vec();

      if self.should_flock {
        let separation = boid.separation(&others);
        let cohesion = boid.cohesion(&others);
        let alignment = boid.alignment(&others);
        let weighted_sum = self.k_cohesion * cohesion + self.k_separation * separation + self.k_alignment * alignment;
        boid.steer(weighted_sum, dt);
      }

      let visible = others
        .iter()
        .filter(|other| boid.is_within_dist(other, 1.5 * boid.vision_radius))
        .collect_vec();

      let num_flashing = visible.iter().filter(|other| other.is_flashing).count();
      // boid.update_activation(0.05 * num_flashing as f32, true);
      boid.is_flashing = false;
      let h = |x: f32| x;
      boid.activation = boid.activation
        + 1. / self.sync_activation_period * dt
        + num_flashing as f32 * self.sync_coupling_constant * h(boid.activation);
      if boid.activation >= 1. {
        // boid.activation %= 1.;
        boid.activation = 0.;
        boid.is_flashing = true;
      }

      boid.update(dt);
      boid.position = wrap_vec(boid.position, self.bbox.bottom_left(), self.bbox.top_right());
    });
    // println!("{current_time}, {max_diff}");
    // dbg!(max_diff, self.is_flocking());
  }
  pub fn render(&self, draw: &Draw) {
    for boid in &self.boids {
      boid.render(&draw);
    }
  }
  pub fn maximum_activation_difference(&self) -> f32 {
    self
      .boids
      .iter()
      .combinations(2)
      .map(|v| (v[0].activation - v[1].activation).abs())
      // distance between eg. 0.8 and 0.2 should be 0.4 not 0.6
      // since the activation is wrapping around
      .map(|diff| if diff > 0.5 { 1. - diff } else { diff })
      .max_by(|a, b| a.partial_cmp(b).unwrap())
      .unwrap()
  }
  pub fn is_flocking(&self) -> bool {
    let mut q = std::collections::VecDeque::new();
    let mut flock = std::collections::HashSet::new();
    q.push_front((0, &self.boids[0]));
    flock.insert(0);
    while let Some((i, boid)) = q.pop_front() {
      for (j, other) in self.boids.iter().enumerate() {
        if i == j || flock.contains(&j) {
          continue;
        }
        if boid.is_within_dist(
          &boid.get_local_boid(other.clone(), self.bbox.w(), self.bbox.h()),
          boid.vision_radius,
        ) {
          flock.insert(j);
          q.push_back((j, &other));
        }
      }
    }
    flock.len() == self.boids.len()
  }
  pub fn is_flocking_stable(&mut self, current_time: f32) -> bool {
    if !self.is_flocking() {
      self.last_non_flocking_time = current_time;
    }

    current_time - self.last_non_flocking_time > 5.
  }
}
