use flock::Flock;

mod boid;
mod flock;

use nannou::{color, prelude::*};

fn main() {
  nannou::app(model).size(1800, 1000).update(update).run();
}

struct Model {
  _window: window::Id,
  previous_time: f32,
  flock: Flock,
  fps: f32,
  fps_update_timer: Timer,
  flocking_times: Vec<f32>,
}

fn model(app: &App) -> Model {
  let _window = app.new_window().view(view).build().unwrap();
  let bbox = app.window_rect();
  let flock = Flock::new(25, 200., 1., 1., 1., 5., 0.5, bbox, 0., Some(rand::thread_rng()));

  Model {
    _window,
    previous_time: 0.,
    fps: 0.,
    flock,
    fps_update_timer: Timer { prev_time: 0. },
    flocking_times: Vec::new(),
  }
}

struct Timer {
  prev_time: f32,
}

impl Timer {
  fn elapsed(&mut self, duration: f32, time: f32) -> bool {
    if time - self.prev_time > duration {
      self.prev_time = time;
      return true;
    }
    false
  }
}

fn get_times(cumulative_times: &Vec<f32>) -> Vec<f32> {
  let mut res = Vec::new();
  let mut prev_val = 0.;
  for &t in cumulative_times {
    res.push(t - prev_val - 5.);
    prev_val = t;
  }
  res
}

fn update(app: &App, model: &mut Model, _update: Update) {
  let current_time = app.time;
  let dt = current_time - model.previous_time;
  model.previous_time = current_time;
  if dt > 0.5 {
    // Update is not called when the window is not visible, which makes dt
    // large. When dt is large the boids will synchronize immediately, which is
    // not the expected behavior.
    return;
  }
  if model.fps_update_timer.elapsed(0.1, current_time) {
    model.fps = 1. / dt;
  }
  model.flock.update(dt, current_time);

  // if model.flock.is_flocking_stable(current_time) {
  //   model.flock.reset(current_time);
  //   model.flocking_times.push(current_time);
  //   // dbg!(current_time);
  //   // app.quit();
  //   if model.flocking_times.len() == 10 {
  //     dbg!(&get_times(&model.flocking_times));
  //     app.quit();
  //   }
  // }
}

fn draw_fps(draw: &Draw, fps: f32, position: Point2) {
  draw
    .text(&format!("{:.2}", fps))
    .font_size(20)
    .color(color::LAWNGREEN)
    .xy(position)
    .left_justify()
    .align_text_bottom()
    .finish();
}

fn view(app: &App, model: &Model, frame: Frame) {
  let draw = app.draw();
  draw.background().color(color::gray(0.1));
  model.flock.render(&draw);
  // Draw fps
  draw_fps(&draw, model.fps, app.window_rect().top_right());
  draw.to_frame(app, &frame).unwrap();
}

#[cfg(test)]
mod tests {
  use itertools::Itertools;
  use nannou::{
    prelude::{Rect, Vec2},
    rand::rand,
  };

  use crate::flock::Flock;

  #[derive(Clone, Copy)]
  enum TestType {
    Flocking,
    NonFlocking,
    Static,
  }

  impl TestType {
    fn to_string(&self) -> String {
      match self {
        Self::Flocking => "flocking",
        Self::NonFlocking => "noflocking",
        Self::Static => "static",
      }
      .into()
    }
  }

  fn create_flock(N: u32, eps: f32, test_type: TestType, seed: u64) -> Flock {
    let bbox = Rect::from_w_h(1800., 1000.);
    let mut rng: rand::rngs::StdRng = rand::SeedableRng::seed_from_u64(seed);
    let mut flock = Flock::new(N, 200., 1., 1., 1., 5., eps, bbox, 0., Some(&mut rng));
    if !matches!(test_type, TestType::Flocking) {
      flock.should_flock = false;
    }
    if matches!(test_type, TestType::Static) {
      // align boids in a grid
      let mut x = 0.;
      let mut y = 0.;
      for boid in flock.boids.iter_mut() {
        boid.velocity = Vec2::ZERO;
        boid.position = Vec2::new(x, y);
        x += 150.;
        if x >= bbox.w() {
          x = 0.;
          y += 150.;
        }
      }
    }
    flock
  }

  #[test]
  fn synchronization_epsilon() {
    let epsilon_values = (0..10).map(|i| 0.05 + i as f32 * 0.05).collect::<Vec<_>>();
    let dt = 1. / 60.;
    for flock_configuration in [TestType::Flocking, TestType::NonFlocking, TestType::Static].iter() {
      for &eps in &epsilon_values {
        println!("{}-eps={eps}", flock_configuration.to_string());
        let path = format!("results/eps-{}/eps-{eps}", flock_configuration.to_string());
        std::fs::create_dir_all(&path).unwrap();
        for seed in 0..10 {
          let mut time_diff_values = Vec::new();
          let mut flock = create_flock(25, eps, *flock_configuration, seed);
          let mut t = 0.;
          while t < 90. {
            flock.update(dt, t);
            let max_diff = flock.maximum_activation_difference();
            time_diff_values.push((t, max_diff));
            t += dt;
          }
          std::fs::write(
            format!("{path}/eps-{seed}.csv"),
            time_diff_values.iter().map(|(t, d)| format!("{t},{d}")).join("\n"),
          )
          .unwrap();
        }
      }
    }
  }
  #[test]
  fn synchronization_N() {
    let N_values = (0..10).map(|i| 5 * i + 5).collect::<Vec<_>>();
    let dt = 1. / 60.;
    for flock_configuration in [TestType::Flocking, TestType::NonFlocking, TestType::Static].iter() {
      for &N in &N_values {
        println!("{}N={N}", flock_configuration.to_string());
        let path = format!("results/N-{}/N-{N}", flock_configuration.to_string());
        std::fs::create_dir_all(&path).unwrap();
        for seed in 0..10 {
          let mut time_diff_values = Vec::new();
          let mut flock = create_flock(N, 0.5, *flock_configuration, seed);
          let mut t = 0.;
          while t < 30. {
            flock.update(dt, t);
            let max_diff = flock.maximum_activation_difference();
            time_diff_values.push((t, max_diff));
            t += dt;
          }
          std::fs::write(
            format!("{path}/N-{seed}.csv"),
            time_diff_values.iter().map(|(t, d)| format!("{t},{d}")).join("\n"),
          )
          .unwrap();
        }
      }
    }
  }
}
