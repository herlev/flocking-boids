use itertools::Itertools;
use nannou::{color::Alpha, prelude::*, Draw};

#[derive(Clone, Debug)]
pub struct Boid {
  pub position: Vec2,
  pub velocity: Vec2,
  pub activation: f32,
  pub vision_radius: f32,
  pub is_flashing: bool,
}

impl Boid {
  pub fn render(&self, draw: &Draw) {
    let color = hsl(self.activation, 1., 0.5);
    let size = 20.;
    let mut a: Alpha<Hsl, f32> = color.into();
    a.alpha = 0.01;
    draw
      .ellipse()
      .xy(self.position)
      .radius(0.5 * self.vision_radius)
      .stroke(a)
      .no_fill()
      .stroke_weight(5.);
    if self.velocity == Vec2::new(0., 0.) {
      draw.ellipse().xy(self.position).radius(size / 2.).color(color);
    } else {
      draw
        .tri()
        .xy(self.position + self.velocity.normalize() * size * 0.9)
        .w_h(size, size)
        .rotate(self.velocity.angle())
        .color(color);
    }
  }

  pub fn is_within_dist(&self, other: &Boid, dist: f32) -> bool {
    self.position.distance(other.position) <= dist
  }

  pub fn steer(&mut self, target: Vec2, dt: f32) {
    if target.length() == 0. {
      return;
    }
    let angle_diff = target.angle() - self.velocity.angle();
    let angle_diff = (angle_diff + PI).rem_euclid(2. * PI) - PI; // shortest angle: -200 deg -> 160 deg
    let max_turning_rate = PI; // radians/s
    let turn = angle_diff.signum() * angle_diff.abs().min(max_turning_rate * dt);
    self.velocity = self.velocity.rotate(turn);
    // let steering_force = sum / diffs.len() as f32 - boid.velocity;
    // let steering_force = (sum.normalize() * SPEED - boid.velocity) * 2.;
    // dbg!(steering_force.length());
    // let steering_force = steering_force.clamp_length_max(200.);
    // boid.apply_force(steering_force);
  }

  pub fn separation(&self, others: &Vec<Boid>) -> Vec2 {
    let visible = others
      .iter()
      .filter(|other| self.is_within_dist(other, 0.5 * self.vision_radius))
      .collect_vec();
    let scaled_diffs = visible
      .iter()
      .map(|other| self.position - other.position)
      .map(|v| v * (0.5 * self.vision_radius - v.length()))
      // .map(|v| v * 0.5 * self.vision_radius / v.length())
      .collect_vec();
    return scaled_diffs.iter().sum();
  }

  pub fn cohesion(&self, others: &Vec<Boid>) -> Vec2 {
    let visible = others
      .iter()
      .filter(|other| self.is_within_dist(other, self.vision_radius))
      .collect_vec();
    if visible.len() == 0 {
      return Vec2::ZERO;
    }
    let avg_pos = visible.iter().map(|other| &other.position).sum::<Vec2>() / visible.len() as f32;
    avg_pos - self.position
  }

  pub fn alignment(&self, others: &Vec<Boid>) -> Vec2 {
    let visible = others
      .iter()
      .filter(|other| self.is_within_dist(other, self.vision_radius))
      .collect_vec();
    if visible.len() == 0 {
      return Vec2::ZERO;
    }
    visible.iter().map(|other| &other.velocity).sum::<Vec2>() / visible.len() as f32
  }

  pub fn update(&mut self, dt: f32) {
    self.position += self.velocity * dt;
  }

  // Makes the following transformation (A is self, B is other):
  // ┌──────┐   ┌──────┐
  // │     A│   │     A│
  // │B     │ ─►│      │B
  // │      │   │      │
  // └──────┘   └──────┘
  pub fn get_local_boid(&self, mut other: Boid, width: f32, height: f32) -> Boid {
    let diff = self.position - other.position;
    if diff.x.abs() > width / 2. {
      other.position.x += diff.x.signum() * width;
    }
    if diff.y.abs() > height / 2. {
      other.position.y += diff.y.signum() * height;
    }
    other
  }
}
